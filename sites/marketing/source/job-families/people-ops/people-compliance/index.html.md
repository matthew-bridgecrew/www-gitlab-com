---
layout: job_family_page
title: "People Compliance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## People Compliance Specialist

<a id="specialist-requirements"></a>

### Job Grade

The People Compliance Specialist is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Support queries relating to compliance, audit and due diligence specific projects in issues, slack and MR's.
- Point of contact for Legal, Security Compliance and Internal Audit-related queries. 
- DRI for all People Group-related compliance projects. Coordinate, collaborate and support the enablement of various compliance projects within this group.   
- Along with the Senior Manager, People Operations implement compliance initiatives and templates that can be utilised for initiatives including: document retention policy for team member information, data protection and annual training on these.
- Ongoing audits and compliance initiatives in Total Rewards, People Operations, and Recruiting to include offer approvals, I9 compliance, onboarding, offboarding, various systems, information transfer.
- Monthly audit on referral bonuses for new hires to ensure accuracy of payouts to referring team members.
- Quarterly audit on documentation added during a Country Conversion, including: mutual termination agreements, accurate probation period notifications, accurate leave accruals. 
- Monitoring legislative changes in countries and states/provinces where we have entities e.g. benefits / Time Off / offer letters / contracts. 
- Administrating the signing of the Code of Conduct, Security Awareness Training and other company wide compliance efforts that are administered and coordinated by the People Group. 
- Monthly Audit/ due diligence on onboarding requirements by jurisdiction as needed, as well as a monthly audit on career mobility issues. 
- Monthly Audit/ due dilligence on offboarding. 
- Collaborate with People Operations Fullstack Engineer and People Data Engineer to implement automation and reporting surrounding this. 
- All country-specific lawful and required reports and compliance initiatives, such as the US-required EEOC report
- Quarterly compliance report presented to Senior Leadership of People Group.
- Engage with vendors, external local Legal counsel and internal Legal counsel.
- Complete ad-hoc projects, reporting, and tasks. 

### Requirements

- You share our [values](/handbook/values/), and work in accordance with those values. 
- The capacity to challenge the status quo with all levels of leadership to ensure compliance.
- The capability to work autonomously and to drive your own performance & development would be important in this role.
- Willing to make People Compliance as open and transparent as possible.
- Proven organizational skills with high attention to detail and the capacity to prioritize.
- The aptitude to work in a fast paced environment with substantial attention to detail is essential.
- High sense of urgency and accuracy.
- Exemplary team player who can jump in and support the team on a variety of topics and tasks.
- Prior extensive experience in HR Compliance, Team Member Data Retention or a People Operations role that included compliance.
- Clear understanding of HR laws in one or multiple countries where GitLab is active.
- Excellent written and verbal communication skills.
- Exceptional customer service skills.
- Enthusiasm for and broad experience with software tools.
- Proven experience to quickly adapt new software tools.
- Willing to work with git and GitLab whenever possible.
- Ability to use GitLab.
- Willingness to work odd hours when needed (for example, to call an embassy in a different continent).
- Bachelor's degree **or** 2 years related experience.
- Experience at a growth-stage tech company is preferred.

### Performance Indicators

Primary performance indicators for this role:

- [100% accuracy of data held in BambooHR for all team members where we have an entity or PEO](/handbook/people-group/people-success-performance-indicators/#compliance-data-retention-and-implementation). 
- [100% accuracy and compliance of all People-related data and documents held in Google Drive](/handbook/people-group/people-success-performance-indicators/#google-drive-documentation).
- [100% implementation of quarterly audits for onboarding, offboarding and career mobility issues](/handbook/people-group/people-success-performance-indicators/#implementation-of-audits-across-team-member-experience-tasks).
These performance indicators are currently across all levels within the People Group.

## Senior Compliance Specialist

### Job Grade

The Senior Compliance Specialist is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Maintain appropriate level of process, program, and policy knowledge in order to assist team members.
- Partner with Legal / Tax on international employment and contractual requirements.
- Team Member Data retention tracking and compliance.
- Along with the Senior Manager, People Operations implement compliance initiatives and templates that can be utilised for initiatives including: document retention policy for team member information, data protection and annual training on these.
- Ongoing audits and compliance initiatives in Total Rewards, People Operations, and Recruiting including offer approvals, I9 Compliance, onboarding, offboarding, various systems, information transfer.
- Oversee monthly audit on referral bonuses for new hires to ensure accuracy of payouts to referring team members.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the team member and/or manager’s experience.
- Announce changes and improvements in the #whats-happening-at-gitlab Slack channel.
- Working closely with the Senior Leadership in the People Group and suggesting iterations on processes and intiatives to ensure we remain compliant.
- Partner with the People Operations Fullstack Engineer on automation and removing mundane tasks from Compliance, Audit and Team Member Data retention.
- Iterate on the onboarding issue and collaborate with the overall team to keep improving onboarding.
- Work with People Operations and the executive team to set up entities or co-employers in new countries as we scale.
- Work with the People Operations Fullstack Engineer on automation-related options for call management and videco conferencing provider.
- Manage Vendor contracts.
- Renewing or ending of contracts and working closely with Procurement on negotiations, and vendor selections.
- Coordinate with Finance on PEO payroll issue.
- Complete ad-hoc projects, reporting, and tasks.
- Collaborating with PTO Ninja on a monthly basis to ensure we continue to iterate and improve on the integration and feedback.

### Performance Indicators

Primary performance indicators for this role:

- [100% accuracy of data held in BambooHR for all team members where we have an entity or PEO](/handbook/people-group/people-success-performance-indicators/#compliance-data-retention-and-implementation). 
- [100% accuracy and compliance of all People-related data and documents held in Google Drive](/handbook/people-group/people-success-performance-indicators/#google-drive-documentation).
- [100% implementation of quarterly audits for onboarding, offboarding and career mobility issues](/handbook/people-group/people-success-performance-indicators/#implementation-of-audits-across-team-member-experience-tasks).
These performance indicators are currently across all levels within the People Group.

## People Compliance Team Lead

### Job Grade

The People Operations Team Lead is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Coach and mentor the People Compliance team to effectively address team member data retention, compliance and audit intiatives.
- Continually audit and monitor compliance with policies and procedures.
- Provide direction on ongoing audits and compliance initiatives in Total Rewards, People Operations, and Recruiting including offer approvals, I9 Compliance, onboarding, offboarding, various systems, information transfer.
- Address any gaps and escalate to Manager, People Operations where required.
- Drive continued automation and efficiency to enhance the team member experience and maintain our efficiency value.
- Announcing changes and improvements in the #whats-happening-at-gitlab Slack channel.
- Driving a positive team member experience throughout their lifecycle with GitLab.
- Quarterly report on trends from exit interview data.
- Partner with all relevant teams in GitLab and PEO's on conversions, to ensure both compliance and ongoing audit initiatives.
- Manage vendor renewals and agreements.
- Partner with Finance Business Partner for budgeting purposes.
- Complete ad-hoc projects, reporting, and tasks.

### Performance Indicators

Primary performance indicators for this role:

- [100% accuracy of data held in BambooHR for all team members where we have an entity or PEO](/handbook/people-group/people-success-performance-indicators/#compliance-data-retention-and-implementation). 
- [100% accuracy and compliance of all People-related data and documents held in Google Drive](/handbook/people-group/people-success-performance-indicators/#google-drive-documentation).
- [100% implementation of quarterly audits for onboarding, offboarding and career mobility issues](/handbook/people-group/people-success-performance-indicators/#implementation-of-audits-across-team-member-experience-tasks).
These performance indicators are currently across all levels within the People Group.

### Hiring Process
- Qualified candidates will be invited to schedule a 30 minute screening call with one of our recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Senior Manager, People Operations
- After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations, Total Rewards, Recruiting and People Partner teams
- After that, candidates will be invited to interview with the Senior Director, People Success
- Finally, our CPO may choose to conduct a final interview

### Career Ladder

The next step in the People Compliance job family is to move to the [People Leadership job family](/job-families/people-ops/people-leadership/).
