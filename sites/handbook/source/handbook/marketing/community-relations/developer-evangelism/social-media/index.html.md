---
layout: handbook-page-toc
title: "Developer Evangelism on Social Media"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Developer Evangelism builds out their thought leadership through social media and community engagement. The tips and strategies shared here are used by team members and can help build your own profile as an evangelist.

## Tools

### Scheduled Content

We use [Buffer](https://buffer.com/) in the [Pro tier](https://buffer.com/pricing/publish) to manage scheduled campaigns:

- Release Evangelism: Share feature insights with personal views. 
- Community best practices: Interesting posts from our Discourse forum.
  - Buffer can consume RSS feeds, for example `https://forum.gitlab.com/c/questions-and-answers.rss`.

Twitter and LinkedIn are the target platforms. Note that you can `@` mention Twitter accounts only, the LinkedIn integration does not match. Buffer also allows to upload/override a social image for better visual appearances.

Buffer can be used to "live tweet" with the `Share now` functionality.

### Live Content

#### Twitter

- [Twitter web app](https://twitter.com/home)
- [TweetDeck](https://tweetdeck.twitter.com/) with the [Better TweetDeck](https://better.tw/) extension for a column based view
  - Auto-completed emojis, GIFs and media preview
  - Add more columns with custom searches, list subscriptions and notifications
- Own curated Twitter lists, for example: [Michael](https://twitter.com/dnsmichi/lists)  
- `#social-media` channel in Slack  

The TweetDeck search allows for logical expressions:

```
gitlab OR gitlabcommit
```

#### LinkedIn

- [Web app](https://www.linkedin.com/)

### Analytics

- Twitter Analytics: `https://analytics.twitter.com/user/USERNAME/home`
- [Tweepi](https://tweepi.com/app/#!/dashboard)
- [Keyhole.co](https://keyhole.co/)
- Weekly update issues shared in Corporate Marketing Results calls.

LinkedIn impressions are counted manually.


## Thought Leadership Strategy

This strategy can help amplify impressions and build out thought leadership.

- Attract more active followers and therefore improve impression numbers.
- Help and educate users
- Analyse profile statistics
  - The why on the most impressions, top media tweet or most engaging tweets
- Follow users who share interesting stories
  - They may follow back, increasing the follower count.
- Retweet with comment and add your own thoughts or a funny emoji.
  - Mix this with "normal" retweets.
- Engage with tweets, like often, add replies and join the discussion.
  - Say `Thanks for sharing :emoji:` whenever needed
  - Share positive vibes
  - Follow the [social media guidelines](/handbook/marketing/social-media-guidelines/)
- Listen to criticism and ignore hate speech. 
- Do not criticize GitLab competitors. 
  - Instead, engage with their communities and learn how to improve.  
- Channel back feedback to product and engineering teams.
- Adopt new ideas with live streaming or community coffee chats. 

### Message Tips

- Keep the message short and appealing. If you have multiple sentences, break them down into a list.
  - Use emojis as list markers
- Use 3-4 hashtags at maximum. Twitter/LinkedIn are not Instagram and use different algorithms to amplify shares.
  - `#devops` and `#devsecops` are good examples, but avoid them to include in every tweet.
- Too many emojis can hide key messages.
- Use an appealing screenshot image or funny animated GIF to make people stop when scrolling. 
- Do not start with an `@` character, this can be hidden as reply and hinder audience reach on Twitter. Escape it with a leading `.` or an emoji.

#### Fast Emojis Workflows

You can use [Alfred's Powerpack with the emoji workflow](https://dev.to/dnsmichi/emojis-everywhere-supercharged-with-alfred-workflows-1o3n) to quickly access common emojis for tweets. 

Alternatively, Better Tweetdeck offers the same auto-complete mechanism known from GitLab and Slack: Start with a `:` and type the emoji name.

### Build your Social Profile

- Add a personal note and allow users to view a window into your life. Be it food, leisure activities, or a quirky habit. 
- Share your impressions and thoughts with `#allremote` and `#remotework`
- Regularly tweet about daily work. Use the hash tag `#LifeAtGitLab` to share insights and funny moments.
- Pick outstanding GitLab features from another stage/group and post about them (could be a blog post, screenshot, etc.). 
  - Share praise in Slack with linking the Tweet/LinkedIn URL.
- If you find something interesting to be shared with our brand account, please share it on Slack in `#social_media_action`. 
- Post something funny, use an animated GIF which relates to a tech story. Something which makes you laugh when you look at it.

### GitLab Evangelism

Everyone can contribute and everyone can become an evangelist for GitLab. Developer Evangelists use these best practices for sharing insights to GitLab with the wider community.

Team work is everything: Tag GitLab team members when you think they could benefit from sharing on social media.

For incoming questions and involving more experts, community advocates and Developer Evangelists can help in Slack in `#community-advocates` or `#developer-evangelism`.

#### Monthly Releases

[GitLab releases](https://about.gitlab.com/releases/) can be overwhelming with many features every month. This strategy requires Developer Evangelists to impersonate our users in Dev, Sec and Ops and know why a specific new GitLab feature may help them.

Workflow:

- Our [TE Bot](/handbook/marketing/community-relations/developer-evangelism/projects/#technical-evangelism-bot) automatically creates and assigns issues.
- DEs review the release blog post and extract their favourites.
- DEs update Buffer and schedule social shares.
- After the campaign is over, social media analytics happen. Example for [GitLab 13.1](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2938#note_374246517).

Tips:

- One key message per day
- Message format: `Problem? -> Solution`
- Target a time window for PT and CEST (9-11am PT)

The key goal here is to match expectations with more impressions, next to learning which feature messages are not well put enough. 

#### Release Management

Example tweets for [GitLab 12.9](https://about.gitlab.com/releases/2020/03/22/gitlab-12-9-released/):

- Pick a nice title and encourage everyone to try to create a new release: `Have you created your first release through the in @gitlab yet? Let's do this!`
- Go to your demo environment and create 1-4 screenshots, including guiding steps (make this a mini tutorial)
- URL to the release blogpost should be included
- Mention @gitlab with the @ before the username
- Use hashtags `#gitlab #releasemanagement`

#### Package

Example tweets for [open-sourcing the registries](https://about.gitlab.com/blog/2020/03/30/new-features-to-core/):

- Pick a cool title to encourage users to solve a use-case for them: `Unstable npm mirrors no more: @gitlab got you covered - the NPM registry will be open sourced.`
- Go to your demo environment and create 1-4 screenshots, including guiding steps (make this a mini tutorial)
- URL to the release blogpost should be included
- Mention @gitlab with the @ before the username
- Use hashtags: `#gitlab #packages #registry`




