# Netlify CMS

## What is it
Netlify CMS is a git based headless CMS system. https://www.netlifycms.org/

## Where is it? 
'sites/marketing/source/admin/'
The config.yml files sets all the configurations for Netlify CMS. This is where you define what content is editable. 
https://www.netlifycms.org/docs/configuration-options/#configuration-file

## Accessing the Admin
As of now, you can only access Netlify CMS while working locally with the repository. 
https://www.netlifycms.org/docs/beta-features/#working-with-a-local-git-repository

1. Run `npx netlify-cms-proxy-server` from the `sites/marketing` directory of the above repository.
2. In seperate window, fire up local server of marketing site `bundle exec middleman serve`
3. Visit http://localhost:4567/admin/

