---
layout: markdown_page
title: "Product Direction - Product Planning"
description: Product Planning enables larger teams and organizations to manage product portfolios, requirements and other complex agile planning needs seamlessly in a single application. 
canonical_path: "/direction/plan/product_planning/"
---

- TOC
{:toc}

## Product Planning

### Overview

The Product Planning group aims to allow for organizations, business groups, and teams to be able to plan in a collaborative manner within the GitLab DevOps platform. In order to accomplish this, our group is focusing on creating a flexible and unified planning experience that teams and organizations of all sizes can successfully navigate and leverage.

### Project Life-cycle

#### Define

Successful projects begin with high level planning, perhaps a business need, or a contractual statement about what the project must accomplish. It is at this level that successful definitions of discrete goals should occur. There are many such ways to complete this, but at some level, jobs to be done and features need to be captured as a single source of truth.

We are in the process of advancing our [Requirements Management](#requirements-management) functionality to ensure that the project definition can be captured and referenced for the life of the project.

#### Decompose

Once the work has been defined, it's time to begin structuring the jobs to be done into logical pieces that can be allocated to teams or groups. Our [Epics](#epics) category allows you to manage your projects efficiently and with less effort by creating logical groups of issues that share a theme, across projects and milestones. To aid in planning, our [Roadmaps](#roadmaps) category allows users to visually plan and map projects into a timeline view. This helps to clearly articulate sequencing and provides an easy method of collaboration with relevant stakeholders.

#### Execute

We recognize that projects are dynamic and that successful projects continue planning throughout the life-cycle of the project. The same [epics](#epics) and [roadmaps](#roadmaps) used to decompose work at a business, portfolio, or project level also allow for successful planning at the sprint or milestone level. Our new [epic swimlanes](https://gitlab.com/groups/gitlab-org/-/epics/3352) functionality will make this easier than ever, allowing teams to not only visualize issues on a board, but their parent epics. 

#### Verify

A project cannot be considered complete until it successfully satisfies the defined goals. Whatever testing looks like in your organization, our advanced CI/CD pipelines, coupled with our emerging [Quality Management](#quality-management) functionality will allow for a simple and unified approach to verifying success. 


### Categories

The following categories fall within the Product Planning group. Listed below each category is a brief description and the upcoming functionality we're building to advance each category. For further information, check out the direction page for each individual category to dive deeper into our mission to strengthen the product planning capabilities of GitLab.


#### Requirements Management
[Requirements Management Direction](/direction/plan/requirements_management/)

Requirements Management aims to allow organizations of all sizes to successfully create and maintain a single source of truth with respect to the definition of a projects. This definition can take whatever form makes the most sense to an organization, be it formal requirements, or high level jobs to be done. Regardless of a projects complexity, GitLab strives to allow for a single source of truth that encourages collaboration among all relevant user personas, and allows for the defined success criteria to be at the center of your organizations DevOps solution. 

##### Top Strategic Focus

Our initial strategic goal for requirements management is to allow it to replace external tooling for software development and test teams. Historically, it has always been cumbersome and time consuming to manually trace requirements implementation and test coverage using currently available tools. Our customers are excited to see an opportunity within GitLab to automate this traceability. Therefore, our top strategy item is to enable teams to bring their requirements into GitLab, implement and test these requirements, and export the requirements with the corresponding coverage data back to their external tools. 

This has three significant benefits for organizations:

1. Their teams can work in an agile continuous testing / delivery mindset, and have their requirements coverage information be collected for them. This raises the teams efficiency tremendously and effectively eliminates the manual steps of tracing requirements to code / test.

1. Since the software teams no longer need to utilize expensive external tools, program costs can be reduced significantly.

1. Systems teams can maintain their existing requirements tool chains for industry conformity and complex analysis (where these tools make sense).


#### Epics
[Epics Direction](/direction/plan/epics/)
 
Epics allow you to manage your portfolio of projects more efficiently and with less effort by tracking groups of issues that share a theme, across projects and milestones. Nest multiple child epics under a parent epic to create deeper work structures that enable more flexible and granular planning.

##### What's Next for Epics

1. [Epic Swimlanes on Boards](https://gitlab.com/gitlab-org/gitlab/-/issues/7371) - MVC in 13.5
1. [Program/Epic Level Boards](https://gitlab.com/groups/gitlab-org/-/epics/2864)

#### Roadmaps
[Roadmaps Direction](/direction/plan/roadmaps/)

Visually plan and map projects in a roadmap that can be used for tracking and communication. GitLab provides timeline-based roadmap visualizations to enable users plan from small time scales (e.g. 2-week sprints for development teams) to larger time scales (e.g. quarterly or annual strategic initiatives for entire departments).

##### What's Next for Roadmaps

1. [Better Filtering on Roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/2923)
1. [Surface Dependencies on Roadmap](https://gitlab.com/gitlab-org/gitlab/-/issues/33587)


#### Quality Management 
[Quality Management Direction](/direction/plan/quality_management)

Our goal for Quality management in GitLab is to allow for uses to track performance of test cases against their different environments over time, allowing for analysis of trends and identifiying critical failures prior to releasing to production.

##### What's next for Quality Management

The first step in building out Quality Management is a scaffolding framework for testing. In particular, we are calling these test cases, and test sessions. These will be first class native objects in GitLab, used to track the quality process of testing itself.

The MVC can be seen at [https://gitlab.com/groups/gitlab-org/-/epics/3852](https://gitlab.com/groups/gitlab-org/-/epics/3852). This work is currently ongoing, and we expect to have full support for test cases in GitLab 13.5, with additional support for test sessions in GitLab 13.6.

#### Service Desk
[Service Desk](/direction/plan/service_desk)

Service desk allows your organization the opportunity to provide an email address to your customers. These customers can send issues, feature requests, comments, and suggestions via email, with no external tools needed. These emails become issues right inside GitLab, potentially even in the same project where you are developing your product or service, pulling your customers directly into your DevOps process.

##### What's next for Service Desk

1. [Allow private comments on all commentable resources](https://gitlab.com/groups/gitlab-org/-/epics/2697)

*The above plan can change at any moment and should not be taken as a hard commitment, though we do try to keep things generally stable. In general, we follow the same [prioritization guidelines](/handbook/product/product-processes/#how-we-prioritize-work) as the product team at large. Issues will tend to flow from having no milestone or epic, to being added to the backlog, to being added to this page and/or a specific milestone for delivery.*


### Product Planning operations

This section is designed to help us all collaborate more effectively, and to provide an overview of our planning strategy.

#### How to collaborate with us

We believe in a world where **everyone can contribute**. We value your contributions, so here are some ways to join in!

[issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARequirements%20Management) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARequirements%20Management)!
* Collaborate by commenting and voting on any of the epics or issues for the categories within this group (below).
* Feel free to share feedback directly via email, Twitter or on a video call.
* Don't hesitate to create an issue for a new feature or enhancement, if one does not already exist.
* Finally, please create an MR against this direction page to make it better!

| Category | Epics | Issues |
| -------- | ----- | ------ |
| Epics | [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AEpics) | [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AEpics) | 
| Roadmaps | [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARoadmaps) | [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARoadmaps) |
| Requirements Management | [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARequirements%20Management) | [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARequirements%20Management) |
| Quality Management | [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AQuality%20Management) | [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AQuality%20Management) |
| Service Desk  | [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AService%20Desk) | [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AService%20Desk) |

### How we prioritize 

Our team uses the standard GitLab prioritization framework for feature requests, enhancements, and bug fixes as described in the [Prioritization](https://about.gitlab.com/handbook/product/product-processes/#how-we-prioritize-work) section of the GitLab Handbook.
