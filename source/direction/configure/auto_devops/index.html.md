---
layout: markdown_page
title: "Category Direction - Auto Devops"
direction: "GiitLab's direction for “Auto DevOps” is to leverage our single application to assist users in every phase of the development and delivery process. Learn more!"
canonical_path: "/direction/configure/auto_devops/"
---

- TOC
{:toc}

## Auto DevOps

Our direction for “Auto DevOps” is to leverage our [single application](https://about.gitlab.com/handbook/product/single-application/) to assist users in every phase of the development and delivery process, implementing automatic tasks that can be customized and refined to get the best fit for their needs.

With the dramatic increase in the number of projects being managed by software teams (especially with the rise of micro-services), it's no longer enough to just craft your code. In addition, you must consider all of the other aspects that will make your project successful, such as tests, quality, security, deployment, logging, monitoring, etc. It's no longer acceptable to add these things only when they are needed, or when the project becomes popular, or when there's a problem to address; on the contrary, all of these things should be available at inception.

[Watch this video of our CEO Sid explaining the importance of Auto DevOps](https://www.youtube.com/watch?v=i7qL1dS5x8g) and follow along with this issue where we are organizing to increase [Auto DevOps adoption](https://gitlab.com/gitlab-com/Product/-/issues/1801). 

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AAuto%20DevOps)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/480) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

We're purusing ways to increase [Auto DevOps adoption](https://gitlab.com/gitlab-com/Product/-/issues/1801).

As the Kubernetes project rapidly matures and some of its API endpoints are deprecated/removed, we must ensure all the components of Auto DevOps currently interacting with its API are updated to ensure its continuing successful function.

[Auto DevOps readiness for Kubernetes 1.16](https://gitlab.com/gitlab-org/gitlab/issues/32720)

## Maturity Plan

This category is currently at the "Minimal" maturity level, and our next maturity target is [Viable](/direction/maturity/). See the [Auto DevOps viable](https://gitlab.com/groups/gitlab-org/-/epics/1333) epic for more info. Deliverables:

- [Auto DevOps readiness for Kubernetes 1.16](https://gitlab.com/gitlab-org/gitlab/issues/32720)

### Group Ownership

Auto Devops ties together several feature from across GitLab [product categories](/handbook/product/categories/). Each individual feature will have its own maturity classification. 

| Feature | Responsible GitLab Group  |
| ------ | ------ |
| Auto Build | [Configure](/handbook/engineering/development/ops/configure/) |
| Auto [Test](/direction/verify/code_testing/)  | [Testing](/handbook/engineering/development/ops/verify/testing/) |
| Auto [Code Quality](/direction/verify/code_quality/)  | [Testing](/handbook/engineering/development/ops/verify/testing/) |
| Auto [SAST](/direction/secure/static-analysis/sast/)  | [Static Analysis](/handbook/engineering/development/secure/static-analysis/) |
| Auto [Secret Detection](/direction/secure/static-analysis/secret-detection/)  | [Static Analysis](/handbook/engineering/development/secure/static-analysis/) |
| Auto [Dependency Scanning](/direction/secure/composition-analysis/dependency-scanning/) | Composition |
| Auto [License Compliance](/direction/secure/composition-analysis/license-compliance/)  | Composition |
| Auto [Container Scanning](/direction/protect/container-scanning/)  | Container Security |
| Auto [Review Apps](/direction/release/review_apps/) | [Progressive Delivery](/handbook/engineering/development/ops/release/progressive-delivery/) |
| Auto [DAST](/direction/secure/dynamic-analysis/dast/)  | Dynamic Analysis |
| Auto [Browser Performance Testing](/direction/verify/web_performance/)  | [Testing](/handbook/engineering/development/ops/verify/testing/) |
| Auto [Load Performance testing](/direction/verify/load_testing/)  | [Testing](/handbook/engineering/development/ops/verify/testing/) |
| Auto [Deploy](/direction/release/continuous_delivery/)  | [Progressive Delivery](/handbook/engineering/development/ops/release/progressive-delivery/) |
| Auto [Monitoring](/direction/monitor/)  | [APM](/handbook/engineering/development/ops/monitor/apm/) |

## Competitive Landscape

While there are "piece-meal" solutions that offer to automate a particular stage, there are no comprehensive tools that offer to address the entire devops lifecycle.

### DeployHQ

DeployHQ offers to "Automatically build and deploy code from your repositories", however, its UX is complex that its deployment targets limited.

[deployhq.com](https://www.deployhq.com)

## Analyst Landscape

There is currently no analyst category that aligns with Auto DevOps.

## Top Customer Success/Sales Issue(s)

[Use Auto DevOps for design.gitlab.com](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/96)

## Top Customer Issue(s)

[Add support for AWS ECS deployments to Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ce/issues/38430)

## Top Internal Customer Issue(s)

[Use Auto DevOps for design.gitlab.com](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/96)

#### Top Vision Item(s) 

- [Disable Auto DevOps at the Group level for gitlab.com](https://gitlab.com/gitlab-org/gitlab-ce/issues/52447) 

- [Composable Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ce/issues/47234)

- [Don't run Auto DevOps when no dockerfile or matching buildpack exists](https://gitlab.com/gitlab-org/gitlab-ce/issues/57483)
